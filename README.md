# KDE Plasma 6 Theme: "Moonever Coffee"

## Introduction

Welcome to the "Moonever Coffee" theme for KDE Plasma 6, a unique blend of artistic inspiration and functional design. Heavily inspired by the ambiance of coffee shops, cafes, Moon and the Balkan phenomenon "Kafana", this theme combines the rich color palette of Gruvbox with a touch of artistic flair, creating a visually appealing and user-friendly environment.

## Features

- **Coffee Shop Inspiration**: Drawing from the cozy and inviting atmosphere of coffee shops and cafes, this theme offers a unique aesthetic that transports you to a warm and welcoming space.
- **Balkan "Kafana" Influence**: Incorporating elements of the Balkan "Kafana", known for its vibrant social life and cultural significance, this theme adds a layer of depth and authenticity to your Plasma experience.
- **Gruvbox Color Scheme**: Leveraging the Gruvbox color scheme, known for its pleasing contrast and easy-on-the-eyes palette, this theme ensures readability and a comfortable user experience.
- **Ease of Use**: Designed with usability in mind, this theme provides a balance between style and functionality, making it easy to navigate and use without compromising on aesthetics.
- **Detailed Design**: While not minimalistic, this theme is rich in detail, offering a visually engaging experience that keeps you coming back for more.

## Installation

1. Download the theme package
2. Extract the downloaded files 
3. Copy "Moonever Coffee" to `~/.local/share/plasma/desktoptheme/`
4. Copy "Moonever-Coffee-Aurorae" to `~/.local/share/aurorae/themes/`
5. Copy "Moonever." to `~/.local/share/color-shemes/`

?. (create the directory if it doesn't exist, set all up in settings).

## Customization

This theme is highly customizable, allowing you to tweak colors, icons, and other elements to match your personal preferences.

## Contributing

Contributions to the "Moonever Coffee" theme are welcome! 

## Credits

- **Coffee Shops and Cafes**: For the inspiration and the cozy atmosphere.
- **Balkan "Kafana"**: For the cultural and social significance.
- **Gruvbox** & **Fancy Color**: For the color scheme and design inspiration.

Enjoy your new "Moonever Coffee" themed KDE Plasma 6 experience!
